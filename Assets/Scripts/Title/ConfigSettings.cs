﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class ConfigSettings : MonoBehaviour {

	public string[] playerOneConfigs = new string[MainConstants.NUMBER_OF_PLAYER_BUTTONS] 	{ "Up_Player1", "Down_Player1", "Left_Player1", "Right_Player1", "Jump_Player1", "Dash_Player1", "Action_Player1" };
	public string[] playerTwoConfigs = new string[MainConstants.NUMBER_OF_PLAYER_BUTTONS] 	{ "Up_Player2", "Down_Player2", "Left_Player2", "Right_Player2", "Jump_Player2", "Dash_Player2", "Action_Player2" };
	public string[] playerThreeConfigs = new string[MainConstants.NUMBER_OF_PLAYER_BUTTONS] { "Up_Player3", "Down_Player3", "Left_Player3", "Right_Player3", "Jump_Player3", "Dash_Player3", "Action_Player3" };
	public string[] playerFourConfigs = new string[MainConstants.NUMBER_OF_PLAYER_BUTTONS] 	{ "Up_Player4", "Down_Player4", "Left_Player4", "Right_Player4", "Jump_Player4", "Dash_Player4", "Action_Player4" };

	public Text[] configButtonInputs = new Text[MainConstants.NUMBER_OF_PLAYER_BUTTONS];


	void Awake() {
		

	}

	void Start () {
		for (int i = 0; i < configButtonInputs.Length; i++) {
			configButtonInputs [i].text = PlayerPrefs.GetString (playerOneConfigs [i]);
		}
	}
	public void buttonPressed(Button button){
		StartCoroutine (waitForButtonInput(button));
	}

	IEnumerator waitForButtonInput(Button button) {
		while (!Input.anyKeyDown) {
			yield return null;
		}
		Text tempText = button.GetComponentInChildren<Text> ();
		tempText.text = Input.inputString;
		checkButtons (tempText);
	}

	public void saveSettings() {
		//Will need to check inputs for bad characters etc
		bool correctInput = true;
		for (int i = 0; i < configButtonInputs.Length; i++) {
			if (configButtonInputs [i].text == "") {
				correctInput = false;
			}
		}
				
		if (correctInput) {
			//Debug.Log ("" + configButtonInputs.Length);
			for (int i = 0; i < configButtonInputs.Length; i++) {
				if (configButtonInputs [i].text == " " || configButtonInputs[i].text == "Space") {
					PlayerPrefs.SetString (playerOneConfigs [i], "Space");
				} else {
					PlayerPrefs.SetString (playerOneConfigs [i], configButtonInputs [i].text.ToUpper ());
				}
				Debug.Log ("" + i + ": " + playerOneConfigs [i]);
			} 
			PlayerPrefs.Save();

		} else {
			//Error!
		}
	}

	public void checkButtons(Text text) {
		for (int i = 0; i < configButtonInputs.Length; i++) {
			if (configButtonInputs [i].text == text.text && !configButtonInputs[i].Equals(text)) {
				configButtonInputs [i].text = "";
			}
		}
	}
}

//
//			Debug.Log (
//				"Left = " + PlayerPrefs.GetString("Left") + 
//				"\nRight = " + PlayerPrefs.GetString("Right") + 
//				"\nUp = " + PlayerPrefs.GetString("Up") + 
//				"\nDown = " + PlayerPrefs.GetString("Down") + 
//				"\nJump = " + PlayerPrefs.GetString("Jump") + 
//				"\nDash = " + PlayerPrefs.GetString("Dash") + 
//				"\nAction = " + PlayerPrefs.GetString("Action")
//			);