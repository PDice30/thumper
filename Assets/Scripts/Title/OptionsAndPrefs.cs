﻿using UnityEngine;
using System.Collections;

public class OptionsAndPrefs : MonoBehaviour {

	public int roundsLength; //In Minutes
	public int eventFrequency; // 1 - 5
	public int powerUpFrequency; // 1 - 9
	public int bombFrequency; // 1 - 9
	public int numberOfRounds; // 1, 3, 5, 7, 9

	public int numberOfPlayers; // 1 - 4

	void Start () {
		DontDestroyOnLoad (gameObject);
	}

	public void setDefaults() {
		roundsLength = 5;
		eventFrequency = 3;
		powerUpFrequency = 5;
		bombFrequency = 5;
		numberOfRounds = 3;
		numberOfPlayers = 2;
	}

}
