﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneManagement : MonoBehaviour {

	// Use this for initialization
	//private GameObject mainButtonPanel;
	public Camera mainCamera;
	private Canvas mainCanvas;
	private Canvas optionsCanvas;
	private Canvas configCanvas;

	public GameObject canvasContainer;

	private Animator cameraAnimator;

	public void Awake() {
		cameraAnimator = mainCamera.GetComponent<Animator> ();
		/*
		mainCanvas = GameObject.Find ("MainCanvas").GetComponent<Canvas>();
		optionsCanvas = GameObject.Find ("OptionsCanvas").GetComponent<Canvas>();
		configCanvas = GameObject.Find ("ConfigCanvas").GetComponent<Canvas>();
		*/
	
	}	
	public void Start() {
		
		//cameraAnimator
		//Might need a coroutine to make sure it all finds everything properly
		//optionsCanvas.enabled = false;
		//configCanvas.enabled = false;
	}

	public void Update() {
		if (Input.GetKeyDown (KeyCode.A)) {
			Vector3 newPos = new Vector3 (canvasContainer.transform.position.x - 40, canvasContainer.transform.position.y, 0);
			canvasContainer.transform.position = newPos;
		}
	}

	public void transitionToMain() {
		transferOptionInformation ();

		SceneManager.LoadScene ("Main");
	}

	public void displayMainCanvas() {
		cameraAnimator.Play("OptionsToMode");
		mainCamera.transform.position = new Vector3 (0, 0, -10f);
	}
	//Disable the canvas object instead?
	public void displayOptionsCanvas() {
		cameraAnimator.Play("ModeToOptions");
		//mainCamera.transform.position = new Vector3 (18.6f, 0, -10f);
	}

	public void displayConfigCanvas() {
		
		mainCamera.transform.position = new Vector3 (0, -10f, -10f);
	}



	public void transferOptionInformation() {
		OptionsAndPrefsUI optionsUI = GameObject.Find ("OptionsAndPrefsUI").GetComponent<OptionsAndPrefsUI>();
		OptionsAndPrefs options = GameObject.Find ("OptionsAndPrefs").GetComponent<OptionsAndPrefs> ();

		options.roundsLength = (int)optionsUI.roundLength;
		options.eventFrequency = (int)optionsUI.eventFrequency;
		options.powerUpFrequency = (int)optionsUI.powerUpFrequency;
		options.bombFrequency = (int)optionsUI.bombFrequency;
		options.numberOfRounds = (int)optionsUI.numberOfRounds;

		//TODO
		options.numberOfPlayers = 2;


	}

}
