﻿using UnityEngine;
using System.Collections;

public class PlayerSettings : MonoBehaviour {

	// Use this for initialization
	void Awake() {
		if (PlayerPrefs.GetInt ("FirstTimeOpening", 1) == 1) {

			PlayerPrefs.SetString ("Up_Player1", "W");
			PlayerPrefs.SetString ("Down_Player1", "S");
			PlayerPrefs.SetString ("Left_Player1", "A");
			PlayerPrefs.SetString ("Right_Player1", "D");
			PlayerPrefs.SetString ("Jump_Player1", "Space");
			PlayerPrefs.SetString ("Dash_Player1", "Q");
			PlayerPrefs.SetString ("Action_Player1", "E");

			PlayerPrefs.SetString ("Up_Player2", "I");
			PlayerPrefs.SetString ("Down_Player2", "K");
			PlayerPrefs.SetString ("Left_Player2", "J");
			PlayerPrefs.SetString ("Right_Player2", "L");
			PlayerPrefs.SetString ("Jump_Player2", "P");
			PlayerPrefs.SetString ("Dash_Player2", "U");
			PlayerPrefs.SetString ("Action_Player2", "O");

			PlayerPrefs.SetInt ("FirstTimeOpening", 0);

			PlayerPrefs.Save ();

			Debug.Log ("First time opening");
		} else {
			Debug.Log ("Not first time opening");
		}
	}	

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
