﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	//TODO: Serialize a lot of fields, convert to private variables but still exposed in editor.
	// Use this for initialization
	public Rigidbody2D rigidBody;
	public GameObject playerInteractionCollider;

	public int playerId;

	Animator anim;

	private string[] playerConfig; 

	//Will need to rewrite a lot of this due to augments
	public bool isAirborne;
	public bool isDashing;

	public float dashTime;
	public float beginDashTime;

	public int currentJumps;
	public int currentDashes;

	public int maxJumps;
	public int maxDashes;

	[Range(2.0f, 50.0f)]
	public float maxVelocity;

	public float accelerationForce;
	public float jumpingForce;

	public float gravity;


	public float verticalSpeed;

	private KeyCode _jump;
	private KeyCode _left;
	private KeyCode _right;
	private KeyCode _up;
	private KeyCode _down;
	private KeyCode _dash;
	private KeyCode _action;

	private GameObject itemNextTo;
	private GameObject itemCarried;

	void Awake () {
		
	}

	void Start () {
		rigidBody = gameObject.GetComponent<Rigidbody2D> ();
		anim = gameObject.GetComponent<Animator> ();
		isDashing = false;
		isAirborne = true;
		maxJumps = 2;
		currentJumps = 2;
		maxDashes = 3;
		currentDashes = 3;
		dashTime = 0.0f;
		maxVelocity = 10f;
		accelerationForce = 20;
		jumpingForce = 700;
		gravity = rigidBody.gravityScale;

		//isCarryingItem = false;

		//Get Button Config

		//_up = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up"));
		//_down = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down"));
		_left = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left_Player" + playerId.ToString()));
		_right = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right_Player" + playerId.ToString()));
		_jump = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Jump_Player" + playerId.ToString()));
		_dash = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Dash_Player" + playerId.ToString()));
		_action = (KeyCode)System.Enum.Parse (typeof(KeyCode), PlayerPrefs.GetString ("Action_Player" + playerId.ToString ()));


	}

	// Update is called once per frame
	void Update () {

		//TODO: While carrying something, the weight of the object should affect the players speed and gravity scale perhaps?

		//Delta Time 
		if (Input.GetKey(_left)) { // Left
			rigidBody.AddForce (new Vector2 (-accelerationForce, 0));
		} else if (Input.GetKey(_right)) { // Right
			rigidBody.AddForce (new Vector2 (accelerationForce, 0));
		}


		//KeyCode.W replaced by player settings for Jump
		//Using a command type?
		if (Input.GetKeyDown(_jump) && currentJumps != 0) {
			if (isAirborne) {
				rigidBody.velocity = new Vector2 (rigidBody.velocity.x, 0);
				rigidBody.AddForce(new Vector2(0, jumpingForce));
				currentJumps -= 1;
			} else {
				rigidBody.AddForce(new Vector2(0, jumpingForce));
				currentJumps -= 1;
				isAirborne = true;
			}

		}

		//Air Dash
		if (Input.GetKeyDown(_dash) && isAirborne && currentDashes != 0 && !isDashing) {
			isDashing = true;
			playerAirdash ();
		}


		if (isDashing) {
			if ((dashTime -= Time.deltaTime) < 0) {
				if (gameObject.GetComponent<ConstantForce2D> () != null) {
					Destroy (gameObject.GetComponent<ConstantForce2D> ());
					rigidBody.gravityScale = gravity;
					isDashing = false;
				}
			}
		}


		if (Input.GetKeyDown (_action)) { 
			// And Not carrying anything
			if (itemNextTo != null && itemCarried == null) {
				//This should be a general object check and then do something based on what the object says to do with it when action is pressed

				if (itemNextTo.GetComponent<ItemController> () != null) {
					itemCarried = itemNextTo;
					itemNextTo = null;
					itemCarried.GetComponent<ItemController> ().pickedUpBy (gameObject);
				}

			} else if (itemCarried != null) {
				itemCarried.GetComponent<ItemController> ().droppedBy (gameObject);
				itemCarried = null;
			}
		}
	

		anim.SetFloat ("PlayerSpeed", rigidBody.velocity.x);

		verticalSpeed = rigidBody.velocity.y;

	}

	//TODO: Could put a trigger right above platforms and a collider on the player's feet when they hit that trigger to reset jumps.


	//Reading these commands can be MISSED because they are in fixed update - only forces that are one offs need to be in normal update, like jump
	void FixedUpdate () {
		/*
		if (Input.GetKey(_left)) { // Left
			rigidBody.AddForce (new Vector2 (-accelerationForce, 0));
		} else if (Input.GetKey(_right)) { // Right
			rigidBody.AddForce (new Vector2 (accelerationForce, 0));
		}
		*/
	
		checkSpeed ();
	}

	void OnCollisionEnter2D (Collision2D coll) {
		if (coll.gameObject.tag == "Ground" && currentJumps != maxJumps) {
			//Debug.Log("Hit ground");
			currentJumps = maxJumps;
			currentDashes = maxDashes;
			isAirborne = false;
		}
		if (coll.gameObject.tag == "PlayerPowerup") {
			getPowerup (coll.gameObject.GetComponent<PlayerPowerup>());
			Destroy (coll.gameObject);
		}
	}
	/* May be costly */
	void OnTriggerStay2D (Collider2D coll) {
		if (coll.gameObject.tag == "Platform" && currentJumps != maxJumps && rigidBody.velocity.y <= 0) {
			//Debug.Log("Hit ground");
			currentJumps = maxJumps;
			currentDashes = maxDashes;
			isAirborne = false;
		}
	}

	//There could be some weird behavior here if someone else exits and item after another has entered the trigger space
	void OnTriggerEnter2D (Collider2D coll) {
		if (coll.gameObject.tag == "Item") {
			itemNextTo = coll.gameObject;
		}
	}
	void OnTriggerExit2D (Collider2D coll) {
		if (coll.gameObject.tag == "Item") {
			itemNextTo = null;
		}
	}

	//Will need to add a check to not stop velocity if thrown by something like a bomb.
	void checkSpeed() {
		
		if (Mathf.Abs(rigidBody.velocity.x) > maxVelocity) {
			if (rigidBody.velocity.x > 0) {
				//Just set it to a new vector?
				Vector2 newVelocity = new Vector2 (maxVelocity, rigidBody.velocity.y);
				rigidBody.velocity = newVelocity;
			} else {
				Vector2 newVelocity = new Vector2 (-maxVelocity, rigidBody.velocity.y);
				rigidBody.velocity = newVelocity;
			}
		}
	}

	void getPowerup(PlayerPowerup powerup) {
		switch (powerup.getPowerUpType()) {
		case PowerUpType.Acceleration:
			accelerationForce += 5;
			break;
		case PowerUpType.Defense:
			//accelerationForce += 5;
			break;
		case PowerUpType.JumpForce:
			jumpingForce += 50;
			break;
		case PowerUpType.MaxJumps:
			maxJumps += 1;
			break;
		case PowerUpType.MaxSpeed:
			maxVelocity += 1;
			break;
		case PowerUpType.Power:
			//accelerationForce += 5;
			break;
		}
	}

	//Commands? Pass back instructions to the player object for buttons

	//Augments - How will they work with commands?  Players have different movement options based on their augments.

	//Testing airdash for fun: 'K' will input air dash
	void playerAirdash() {
		float currentDirection = rigidBody.velocity.x;
		rigidBody.velocity = new Vector2 (0, 0);
		gameObject.AddComponent<ConstantForce2D> ();


		if (Input.GetKey (_left)) { // Dash Left
			gameObject.GetComponent<ConstantForce2D> ().force = new Vector2 (-150, 0);
		} else if (Input.GetKey (_right)) { // Dash Right
			gameObject.GetComponent<ConstantForce2D> ().force = new Vector2 (150, 0);
		} else { // Dash the direction they are currently traveling
			if (currentDirection < 0) {
				gameObject.GetComponent<ConstantForce2D> ().force = new Vector2 (-150, 0);
			} else {
				gameObject.GetComponent<ConstantForce2D> ().force = new Vector2 (150, 0);
			}
		}


		rigidBody.gravityScale = 0;

		//Add a timer
		//beginDashTime = Time.time;

		//Dash Time will be set by Augment as well as Dash Speed, # of dashes etc.
		dashTime = 0.5f;
		currentDashes -= 1;


	}

	/*
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int id) {
		playerId = id;
	}
*/

	//TODO: Make sure everything is being passed correctly and look into the PlayerPowerup Script
}

//
//Debug.Log ("Left = " + _left.ToString () + 
//	"\nRight = " + _right.ToString () + 
//	"\nUp = " + _up.ToString () + 
//	"\nDown = " + _down.ToString () + 
//	"\nJump = " + _jump.ToString () + 
//	"\nDash = " + _dash.ToString () + 
//	"\nAction = " + _action.ToString ()
//);