﻿using UnityEngine;
using System.Collections;

public enum PowerUpType : int {
	MaxSpeed = 0,
	Acceleration,
	MaxJumps,
	JumpForce,
	Power,
	Defense
}
