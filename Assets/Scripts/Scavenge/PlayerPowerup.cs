﻿using UnityEngine;
using System.Collections;

public class PlayerPowerup : MonoBehaviour {

	// Use this for initialization
	public PowerUpType itemType;
	private Sprite sprite;

	void Start () {
		//Debug.Log (itemType);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
		gameObject.GetComponent<SpriteRenderer> ().sprite = sprite;

	}
	public Sprite getSprite() {
		return sprite;
	}

	public void setPowerUpType(PowerUpType itemType) {
		this.itemType = itemType;
	}
	public PowerUpType getPowerUpType() {
		return itemType;
	}

}
