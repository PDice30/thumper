﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ScavengeRoundController : MonoBehaviour {

	public GameObject playerPrefab;
	public Text minutesText;
	public Text secondsText;
	public OptionsAndPrefs optionsAndPrefs;
	private float roundTime;
	private float timeToNextSecond;

	public AudioSource audioSource;

	void Awake() {
		//Check for Null
		GameObject optionsAndPrefsObject;
		if (GameObject.Find ("OptionsAndPrefs") != null) {
			optionsAndPrefsObject = GameObject.Find ("OptionsAndPrefs");
		} else {
			optionsAndPrefsObject = new GameObject ();
			optionsAndPrefsObject.AddComponent<OptionsAndPrefs> ();
			optionsAndPrefsObject.GetComponent<OptionsAndPrefs> ().setDefaults ();
		}
		optionsAndPrefs = optionsAndPrefsObject.GetComponent<OptionsAndPrefs> ();

		//audioSource.clip = 
	}
	// Use this for initialization
	void Start () {
		for (int i = 1; i < optionsAndPrefs.numberOfPlayers + 1; i++) {
			playerPrefab.GetComponent<PlayerController> ().playerId = i;
			//Put Locations
			Instantiate(playerPrefab, new Vector3(i * 3, 0, 0), Quaternion.identity);
		}

		roundTime = optionsAndPrefs.roundsLength * 60;
		minutesText.text = optionsAndPrefs.roundsLength.ToString ();
		secondsText.text = "00";
		timeToNextSecond = 1f;
	}
	
	// Update is called once per frame
	void Update () {
		//Countdown timer, check for round over
		timeToNextSecond -= Time.deltaTime;

		if (timeToNextSecond <= 0) {
			roundTime -= 1;
			calculateText ();


			timeToNextSecond = 1f;
		}

		if (roundTime <= 0) {
			//Transition to Next Round, for testing purposes this is going back to the home screen
			if (GameObject.Find ("OptionsAndPrefs") != null) {
				Destroy(GameObject.Find("OptionsAndPrefs"));
			}
			SceneManager.LoadScene("Title");

		}

	}

	void calculateText() {
		float newSeconds = Mathf.Round (roundTime % 60);
		if (newSeconds == 59) {
			float newMinutes = Mathf.CeilToInt(roundTime / 60) - 1;
			minutesText.text = newMinutes.ToString ();
		}

		if (newSeconds < 10) {
			string placeholderSeconds = newSeconds.ToString ();
			placeholderSeconds = ("0" + placeholderSeconds);
			secondsText.text = placeholderSeconds;
		} else {
			secondsText.text = newSeconds.ToString ();
		}


	}

}
