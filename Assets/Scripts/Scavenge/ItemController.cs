﻿using UnityEngine;
using System.Collections;

//Probably Should make a template and subclass all of this
public class ItemController : MonoBehaviour {


	public Rigidbody2D rigidBody; // Set in Inspector
	public BoxCollider2D coll;
	public AudioSource audioSource;

	public GameObject carryingPlayer;


	void Start () {

	}


	void Update () {
		if (carryingPlayer != null) {
			//This will obviously need to be changed
			transform.position = new Vector2(carryingPlayer.transform.position.x, carryingPlayer.transform.position.y + .5f);
		}
	}

	public void pickedUpBy(GameObject player) {
		carryingPlayer = player;
		//Animation will trigger to move the box

		coll.isTrigger = true;
		transform.position = player.transform.position;
		transform.position = new Vector2 (carryingPlayer.transform.position.x, carryingPlayer.transform.position.y + .5f);
		audioSource.PlayOneShot (audioSource.clip);

	}

	public void droppedBy(GameObject player) {
		//This doesn't get played if it gets destroyed first (in assimilator)
		//audioSource.PlayOneShot (audioSource.clip);
		coll.isTrigger = false;
		rigidBody.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		carryingPlayer = null;

	}

	void OnTriggerStay2D(Collider2D coll) {
		if (coll.gameObject.tag == "Assimilator" && carryingPlayer == null) {
			Destroy (gameObject);
		}
	}
}
